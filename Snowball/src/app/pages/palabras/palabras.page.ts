import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import {Router} from '@angular/router'

@Component({
  selector: 'app-palabras',
  templateUrl: './palabras.page.html',
  styleUrls: ['./palabras.page.scss'],
})
export class PalabrasPage implements OnInit {

  jug = null;
  num : number;
  suma = 0;
  jugadores : number[] = [];
  words : string[];

  constructor(private activatedRoute : ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.jug=this.activatedRoute.snapshot.paramMap.get("param");
    console.log(this.jug)
    this.num = Number(this.jug);
    this.suma = this.num + 10;

    for(var i = 0; i<this.num; i++){
      var j = i+1;
      this.jugadores[i]=j;
    }
    console.log(this.jugadores)
    //Creamos el array donde se van a guardar las palabras
    
  }

}
