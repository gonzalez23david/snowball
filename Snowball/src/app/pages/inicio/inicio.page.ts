import { Component, OnInit } from '@angular/core';
import { ValueAccessor } from '@ionic/angular/directives/control-value-accessors/value-accessor';
import { Router } from '@angular/router'

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  
  jug : number;
  frango:boolean = false;
  constructor(private router : Router) {
    
   }

  ngOnInit() {
    
  }

  aceptar(){
    console.log(this.jug)
    if(this.jug>=2 && this.jug<=8){
      console.log("Todo Correcto");
      this.frango=false;
      this.router.navigate(['/palabras', this.jug])
    }else{
      console.log("Fuera de rango")
      this.frango=true;
    }
  }

}
